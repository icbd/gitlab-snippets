# GitLab Snippets

This extension primarily caters to GitLab developers.

I pre-scanned [GitLab's code repository](https://gitlab.com/gitlab-org/gitlab.git), identifying frequently occurring method names, variable names, and chained calls.

![Demo](readme-demo.png)

## How to scan

The tool for creating snippets is located at the `bin/scanner.rb`.
You can customize your own snippets according to your needs.

```sh
cd bin
chmod +x scanner.rb
# ./scanner.rb <repo-path> [--save]
CODE_LINE_SIZE_THRESHOLD=15 COUNTER_FILTER_LIMIT=10 PREFIX_COUNT=4 ./scanner.rb ~/Downloads/gitlab-master --save > counter.csv
```

The scanning results will be sorted in reverse order of frequency and reverse order of symbol length, like this:

|symbol|count|
|---|---|
|...|...|
|author_username                                                                  | 207 |
|let_it_be(:project) { create(:project, :public) }                                | 206 |
|project.add_reporter(user)                                                       | 206 |
|repository_size_limit                                                            | 206 |
|replicable_name                                                                  | 206 |
|group_with_plan                                                                  | 206 |
|...|...|

If you use the `--save` parameter, the tool will save a snippets file (in `JSON` format) to the current directory.

You can define several parameters through environment variables:

|ENV Variable|Feature|
|---|---|
|CODE_LINE_SIZE_THRESHOLD|The minimum symbol length|
|COUNTER_FILTER_LIMIT|The minimum number of occurrences for a symbol|
|PRINTER_WIDTH|The number of spaces reserved for each symbol during formatting|
|PREFIX_COUNT|The number of characters for the snippets prompt|

## Release

Code base: [8c9b6b98 master 2024-02-21](https://gitlab.com/gitlab-org/gitlab/-/tree/8c9b6b98a98fc277ae6ecde2de69b88d74e8076c)
