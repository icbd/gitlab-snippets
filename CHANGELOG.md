# Change Log

## [0.1.0]

- 2024-02-21 Scanner tool and GitLab Snippets based on [8c9b6b98 master 2024-02-21](https://gitlab.com/gitlab-org/gitlab/-/tree/8c9b6b98a98fc277ae6ecde2de69b88d74e8076c)

## [0.1.1]

- 2024-02-21 Refactor prefixes
