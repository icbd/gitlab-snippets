#!/usr/bin/env ruby

require 'json'

# ./scanner.rb <repo-path> [--save]
class Scanner
  CODE_LINE_SIZE_THRESHOLD = 12
  COUNTER_FILTER_LIMIT = 5
  PRINTER_WIDTH = 80
  PREFIX_COUNT = 4

  # def code_line_size_threshold
  # def counter_filter_limit
  # def printer_width
  # def prefix_count
  [:CODE_LINE_SIZE_THRESHOLD, :COUNTER_FILTER_LIMIT, :PRINTER_WIDTH, :PREFIX_COUNT].each do |const_method|
    define_method(const_method.downcase) do
      (ENV[const_method.to_s] || self.class.const_get(const_method)).to_i
    end
  end

  attr_reader :repo_path, :counter
  attr_accessor :sorted # [[k_str, v_num], ...]
  def initialize(repo_path)
    raise "Directory `#{repo_path}` does not exist!" unless Dir.exist?(repo_path)
    @repo_path = repo_path
    @counter = Hash.new(0)
  end

  def scan
    iterator do |line|
      line_handler(line)
    end

    @sorted = counter.sort_by { |k, v| [-v, -k.length] }
  end

  def show(minimum = counter_filter_limit)
    sorted.each do |k, v|
      break if v < minimum

      printf "%-*s , %s\n", printer_width, k, v
    end
  end

  def outputs(minimum = counter_filter_limit)
    file_name = "gitlab.code-snippets"
    raise "The snippets file already exists: `#{file_name}`. Remove it first." if File.exist?(file_name)

    File.open(file_name, "a") do |file|
      file.puts "{"
      is_first = true
      sorted.each do |k, v|
        break if v < minimum

        json_str = {
          k => {
            prefix: prefixes(k),
            body: [k] }
        }.to_json
        file.print(is_first ? "" : ",\n")
        file.print json_str[1...-1] # remove `{` and `}`

        is_first = false
      end
      file.puts "\n}"
    end
  end

  private

  def line_handler(line)
    line = line.delete_prefix("#")
               .strip
               .delete_prefix("def ")
    return unless line.size >= code_line_size_threshold

    counter[line] += 1 if /^[a-zA-Z]/.match?(line)

    line.split(/[^a-zA-Z0-9_.]+/).each do |fragment|
      counter[fragment] += 1 if fragment.size >= code_line_size_threshold
    end
  end

  def iterator
    Dir.glob(File.join(repo_path, '**', '*.rb')).each do |file|
      File.foreach(file) do |line|
        code_line = line.strip
        yield code_line if code_line.size >= code_line_size_threshold
      end
    end
  end

  # item: project.repository.raw_repository
  # => ["pro", "project.", "project.repository."]
  def prefixes(item)
    prefix = item[0...prefix_count]
    prefixes = [prefix, prefix.downcase].uniq

    if item.include?('.')
      split_item = item.split('.')

      # skip last one
      (split_item.size - 1).times do |item_idx|
        chained_call_prefix = split_item.slice(0, item_idx + 1).join(".")
        prefixes << "#{chained_call_prefix}."
      end
    end

    prefixes
  end
end

scanner = Scanner.new(ARGV[0].to_s)
scanner.scan
scanner.show
scanner.outputs if ARGV[1] == "--save"
